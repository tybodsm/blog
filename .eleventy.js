const xmlFiltersPlugin = require('eleventy-xml-plugin');
const sass = require("sass");
const path = require("node:path");
const syntaxHighlight = require("@11ty/eleventy-plugin-syntaxhighlight");

const markdownIt = require("markdown-it");
const markdownItFootnote = require("markdown-it-footnote");
const markdownItAlign = require("markdown-it-align");

const options = {
    html: true, // Enable HTML tags in source
    breaks: false,  // Convert '\n' in paragraphs into <br>
    linkify: true // Autoconvert URL-like text to links
};
const markdownLib = markdownIt(options).use(markdownItFootnote);

function Counter(array) {
    var count = {};
    array.forEach(val => count[val] = (count[val] || 0) + 1);
    return count;
}

module.exports = function (eleventyConfig) {
    // eleventyConfig.addPlugin(mathjaxPlugin);
    // Markdown Footnotes
    eleventyConfig.setLibrary("md", markdownLib);

    eleventyConfig.addPlugin(xmlFiltersPlugin)
    eleventyConfig.addPlugin(syntaxHighlight);

    eleventyConfig.addPassthroughCopy("assets/js/*")
    eleventyConfig.addPassthroughCopy("assets/img/*")
    eleventyConfig.addPassthroughCopy("assets/css/*")

    // Recognize Sass as a "template languages"
    eleventyConfig.addTemplateFormats("scss");

    // Compile Sass
    eleventyConfig.addExtension("scss", {
        outputFileExtension: "css",
        compileOptions: {
            /* 
                Disable caching of `.scss` files, for good measure.
            */
            cache: false,
            permalink: function (permalink, inputPath) {
                let parsed = path.parse(inputPath);
                if (parsed.name.startsWith("_")) {
                    return false;
                }
                return "assets/css/" + parsed.name + ".css"
            }   
        },
        compile: async function (inputContent, inputPath) {
            // Skip files like _fileName.scss
            let parsed = path.parse(inputPath);
            if (parsed.name.startsWith("_")) {
                return;
            }
            
            // Run file content through Sass
            let result = sass.compileString(inputContent, {
                loadPaths: [parsed.dir || "."],
                sourceMap: false, // or true, your choice!
            });

            // Allow included files from @use or @import to
            // trigger rebuilds when using --incremental
            this.addDependencies(inputPath, result.loadedUrls);

            return async () => {
                return result.css;
            };
        },
    });

    eleventyConfig.addCollection("tagsList", collections => {
        const tags = collections
            .getAll()
            .reduce((tags, item) => tags.concat(item.data.tags), [])
            .filter(tag => !!tag && !["post", "all"].includes(tag))
            .sort()
        return Array.from(new Set(tags)).map(tag => ({
            title: tag,
            count: collections.getFilteredByTag(tag).length,
        }))
    })

    return {
        dir: {
            layouts: "_layouts"
        }
    }
};
